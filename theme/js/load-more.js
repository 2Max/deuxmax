/**
 * https://github.com/aspirethemes/Casper
 */

/* globals jQuery, document */
(function ($) {
  "use strict";

  var $document = $(document);

  $document.ready(function () {
    const pagination_next_url = $('link[rel=next]').attr('href'), $load_posts_button = $('.js-load-posts-button');

    $load_posts_button.click(function(e) {
      e.preventDefault();
      const request_next_link = pagination_next_url.split(/page/)[0] + 'page/' + pagination_next_page_number + '/';
      $.ajax({
        url: request_next_link,
        beforeSend: function() {
          $load_posts_button.attr('disabled', true);
        }
      }).done(function(data) {
        const posts = $('.js-load-posts > .js-load-post', data);

        $load_posts_button.attr('disabled', false);

        $('.js-load-posts').append(posts);

        pagination_next_page_number++;

        // If you are on the last pagination page, add the disabled attribute
        if (pagination_next_page_number > pagination_available_pages_number) {
          $load_posts_button.attr('disabled', true);
        }
      });
    });
  });

  // ==============
  // Ajax Load More
  // ==============
})(jQuery);
