$().ready(function () {
  var menu = $('.menu-mobile');
  $('.cta-menu-mobile').on('click', function(event) {
    event.stopPropagation();
    if(menu.hasClass('active')) {
      menu.removeClass('active');
    } else {
      menu.addClass('active');
    }
  });
});
